# -*- coding: utf-8 -*-

class Marble:
    def __init__(self, value):
        self.value = value
        self.next = None
        self.prev = None


class Game:
    def __init__(self, nplayers, nmarbles):
        self.currentMarble = Marble(0)
        self.currentMarble.next = self.currentMarble
        self.currentMarble.prev = self.currentMarble
        self.players = [0] * nplayers
        self.nplayers = nplayers
        self.nmarbles = nmarbles
        
    def play(self):
        nextMarble = 0        
        while nextMarble < self.nmarbles:
            for i in range(nplayers):
                nextMarble += 1
                if nextMarble > self.nmarbles:
                    break
                
                if nextMarble % 23 != 0:
                    self.currentMarble = self.addMarble(nextMarble)
                else:
                    self.players[i] += nextMarble
                    self.players[i] += self.removeMarble()
                
        return max(self.players)
                
    def addMarble(self, value):
        pos = self.currentMarble.next.next
        newMarble = Marble(value)
        newMarble.next = pos
        newMarble.prev = pos.prev
        pos.prev.next = newMarble
        pos.prev = newMarble
        return newMarble
        
    def removeMarble(self):
        pos = self.currentMarble
        for i in range(7):
            pos = pos.prev
        self.currentMarble = pos.next
        pos.next.prev = pos.prev
        pos.prev.next = pos.next        
        value = pos.value
        return value
    

raw = []
with open("input9.txt") as infile:
    raw = infile.read().rstrip("\n").splitlines()

for s in raw:
    nplayers = int(s.split()[0])
    nmarbles = int(s.split()[6])
    
    game = Game(nplayers, nmarbles)
    print(game.play())
    
    game = Game(nplayers, nmarbles*100)
    print(game.play())