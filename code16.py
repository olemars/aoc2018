# -*- coding: utf-8 -*-

from CPU import CPU

cpu = CPU()
opcodes = {i:list(cpu.instr) for i in range(16)}
instr = {i:[] for i in cpu.instr}

def testop(before, after, op):
    global cpu
    global opcodes
    passcount = 0
    cpu.reg = before
    for inst in cpu.instr:
        cpu.reg = list(before)
        inst(op[1], op[2], op[3])
        if cpu.reg == after:
            passcount += 1
            if op[0] not in instr[inst]:
                instr[inst].append(op[0])
        elif inst in opcodes[op[0]]:
            opcodes[op[0]].remove(inst)
            
    return passcount

def postprocess():
    global opcodes
    global instr
    found = 0
    while found < 16:
        for o in sorted(opcodes, key=lambda x: len(opcodes[x])):
            if len(opcodes[o]) == 1:
                if cpu.opcode[o] is None:
                    cpu.opcode[o] = opcodes[o][0]
                    found += 1
              
        for i in sorted(instr, key=lambda x: len(instr[x])):
            if len(instr[i]) == 1:
                op = instr[i][0]
                if cpu.opcode[op] is None:
                    cpu.opcode[op] = i
                    found += 1
                    
        for c in cpu.opcode: 
            if cpu.opcode[c] is not None:          
                for o in opcodes:
                    if cpu.opcode[c] in opcodes[o]:
                        opcodes[o].remove(cpu.opcode[c])  
                for i in instr:
                    if c in instr[i]:
                        instr[i].remove(c) 

with open("input16.txt") as infile:
    line = infile.readline().rstrip()
    before = []
    op = []
    after = []
    blanks = 0
    answer = 0
    while True:
        if len(line) > 0:
            blanks = 0
            if line.startswith("B"):
                before = [int(i) for i in line.lstrip("Before: ").strip("[]").split(", ")]
            elif line.startswith("A"):
                after = [int(i) for i in line.lstrip("After:  ").strip("[]").split(", ")]
            else:
                op = [int(i) for i in line.split(" ")]
        else:
            blanks += 1
            if blanks > 1:
                break
            else:
                if testop(before, after, op) >= 3:
                    answer += 1
        line = infile.readline().rstrip()
    print(f"p1: {answer}")
    
    postprocess()
    cpu.reg = [0,0,0,0]
    for l in infile.readlines():
        l = l.strip()
        if len(l) > 0:
            op = [int(i) for i in l.split(" ")]
            cpu.opcode[op[0]](op[1], op[2], op[3])
    print(f"p2: {cpu.reg[0]}")
