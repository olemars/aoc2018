# -*- coding: utf-8 -*-

from collections import deque
from itertools import islice
from math import floor
import array

tests = (5,9,18,2018)


runlen = 0
runstr = ""
with open("input14.txt") as infile:
    runstr = infile.read().rstrip("\n")
    runlen = int(runstr)


def checklast(last):
    for i in range(len(last)):
        if runstr[i] != last[i]:
            return False
    return True

numelves = 2
elves = {i:i for i in range(numelves)}
anslen = 10
init = [3,7]
results = array.array("i",init)
last = deque([str(v) for v in init], maxlen=len(runstr))

def run():
    i = 1
    totrecipes = len(init)
    while i:
        i += 1
        score = 0
        for elf in elves:
            elves[elf] = (elves[elf] + 1 + results[elves[elf]]) % len(results)
            score += results[elves[elf]]
            
        pending = []    
        while score > 0:
            pending.append(score % 10)
            score = floor(score / 10)
        if len(pending) == 0:
            pending.append(0)
        elif len(pending) > 1:
            pending.reverse()
        for x in pending:
            totrecipes += 1
            results.append(x)
            last.append(str(x))
            if checklast(last):
                print(totrecipes-len(runstr), results[totrecipes - len(runstr):])
                return
            
        #test
#        if i-anslen in tests:
#            print(i-anslen,list(islice(results, i-anslen, i)))
#        if i-anslen == runlen:
#            print(i-anslen,list(islice(results, i-anslen, i)))
#            print("".join(str(x) for x in list(islice(results, i-anslen, i))))
            
run()    