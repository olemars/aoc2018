# -*- coding: utf-8 -*-
import bisect 

raw = []

with open("input7.txt") as infile:
    raw = infile.read().splitlines()
    
class Step:
    def __init__(self, stepid):
        self.stepid = stepid
        self.prev = []
        self.next = []
        self.time = 0
        self.done = False
        
    def __lt__(self, other):
        return self.stepid < other.stepid
    def __le__(self, other):
        return self.stepid <= other.stepid
    def __gt__(self, other):
        return self.stepid > other.stepid
    def __ge__(self, other):
        return self.stepid >= other.stepid
    def __eq__(self, other):
        return self.stepid == other.stepid
    def __ne__(self, other):
        return not self.__eq__(other)
        
    def isready(self):
        for s in self.prev:
            if s.done == False:
                return False
        return True
    
pending = {}

for s in raw:
    stepid = s[s.find("Step ") + 5]
    nextstepid = s[s.find("step ") + 5]
    if pending.get(stepid):
        step = pending[stepid]
    else:
        step = Step(stepid)
        pending[stepid] = step
    
    if pending.get(nextstepid):
        nextstep = pending[nextstepid]
    else:
        nextstep = Step(nextstepid)
        pending[nextstepid] = nextstep
    
    nextstep.time = 60 + (ord(nextstepid) - ord("A")) + 1
    step.time = 60 + (ord(stepid) - ord("A")) + 1
    
    step.next.append(nextstep)
    nextstep.prev.append(step)
    
ready = []
running = []
done = []
order = ""

elves = 5
secs = 0
while len(pending.items()) > 0 or len(ready) > 0 or len(running) > 0:
    if len(running) < elves:
        for s in list(pending):
            if pending[s].isready():
                bisect.insort_left(ready, pending.pop(s))
        
        while len(ready) > 0 and len(running) < elves:
            runstep = ready.pop(0)
            running.append(runstep)
        
    if len(running) > 0:
        justdone = []
        secs += 1
        for step in running:
            step.time -= 1
            if step.time <= 0:
                step.done = True
                bisect.insort_left(justdone, step)

        for step in justdone:
            print(len(justdone), secs, step.stepid) 
            running.pop(running.index(step))
            done.append(step)
            order += step.stepid
            
        
print(order)
print(secs) 