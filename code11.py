# -*- coding: utf-8 -*-

import numpy as np
from math import floor

serial = []
with open("input11.txt") as infile:
    serial = [int(i) for i in infile.read().rstrip("\n").split("\n")]

gridsize = 300

grid = np.ndarray(shape=(gridsize,gridsize), dtype=np.int32)
cell = np.ndarray(shape=(gridsize,gridsize), dtype=np.int32)
for s in serial:
    for i in range(gridsize):
        for j in range(gridsize):
            grid[i,j] = (floor((((i+10)*j)+s)*(i+10)/100) % 10) - 5
    cellsize = 3
    cellcount = gridsize - cellsize + 1
    #cell = np.ndarray(shape=(cellcount,cellcount), dtype=np.int32)
    for i in range(cellcount):
        for j in range(cellcount):
            cell[i,j] = grid[i:i+cellsize,j:j+cellsize].sum()
    print(np.unravel_index(cell[:cellcount,:cellcount].argmax(), cell[:cellcount,:cellcount].shape))
    
    maxpower = 0
    maxindex = []
    size = 0
    for cellsize in range(1,gridsize):
        cellcount = gridsize - cellsize + 1
        #cell = np.ndarray(shape=(cellcount,cellcount), dtype=np.int32)
        for i in range(cellcount):
            for j in range(cellcount):
                cell[i,j] = grid[i:i+cellsize,j:j+cellsize].sum()
        if cell.max() > maxpower:
            maxpower = cell.max()
            maxindex = list(np.unravel_index(cell[:cellcount,:cellcount].argmax(), cell[:cellcount,:cellcount].shape))
            size = cellsize
    print(maxindex,size)