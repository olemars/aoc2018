# -*- coding: utf-8 -*-

from copy import deepcopy

def getneighbors(grid, dim, y, x):
    return [grid[i][j]for i in range(max(y-1, 0), min(y+2, dim)) for j in range(max(x-1, 0), min(x+2, dim)) if not(j == x and i == y)]

with open("input18.txt") as infile:
    raw = infile.readlines()
    start = [[c for c in l.rstrip()] for l in raw]
    grids = [deepcopy(start), deepcopy(start)]
    dim = len(start)

#    print(0)
#    for i in range(dim):
#        print("".join(grids[0][i]))
    
    totals = [0]
    dur=601
    for t in range(1,dur+1):
        grids.reverse()
        totyard = 0
        tottrees = 0
        for i in range(dim):
            for j in range(dim):
                state = grids[0][i][j]
                neighbors = getneighbors(grids[0], dim, i, j)
                grids[1][i][j] = state
                if state == ".":
                    if neighbors.count("|") >= 3:
                        grids[1][i][j] = "|"
                elif state == "|":
                    if neighbors.count("#") >= 3:
                        grids[1][i][j] = "#"
                elif state == "#":
                    if neighbors.count("#") >= 1 and neighbors.count("|") >= 1:
                        grids[1][i][j] = "#"
                    else:
                        grids[1][i][j] = "."
                        
            totyard += grids[1][i].count("#")
            tottrees += grids[1][i].count("|")
            
        total = totyard * tottrees
        totals.append(total)
            
            #print("".join(grids[1][i]))
            
        if t == 10:                
            print("p1: ", totyard * tottrees)
        
    zoi = totals[500:]
    peak1 = zoi.index(max(zoi))
    peak2 = peak1 + zoi[peak1+1:].index(max(zoi))
    interval = zoi[peak1:peak2+1]
    print("p2: ", interval[(1000000000 - (500+peak1)) % len(interval)])
    