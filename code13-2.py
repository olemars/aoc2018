# -*- coding: utf-8 -*-


import numpy as np
from matplotlib import pyplot as plt
from collections import deque
from matplotlib import animation

tr = str.maketrans(" |-/+\\v^<>", "0123456789")
ref = {" ":0,"|":1,"-":2,"/":3,"+":4,"\\":5,"v":6,"^":7,"<":8,">":9}

class Cart:
    def __init__(self, loc, dir):
        self.dir = dir
        self.loc = list(loc)
        self.turn = deque(["r", "s", "l"])
        
    def __lt__(self, other):
        return self.loc < other.loc
    def __le__(self, other):
        return self.loc <= other.loc
    def __gt__(self, other):
        return self.loc > other.loc
    def __ge__(self, other):
        return self.loc >= other.loc
    def __eq__(self, other):
        return self.loc == other.loc
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def move(self):
        if self.dir == ref["^"]:
            self.loc[0] -= 1
        if self.dir == ref["v"]:
            self.loc[0] += 1
        if self.dir == ref["<"]:
            self.loc[1] -= 1
        if self.dir == ref[">"]:
            self.loc[1] += 1
                
    def checkdir(self):
        loc = tuple(self.loc)
        if self.dir == ref["^"]:
            if grid[loc] == ref["/"]:
                self.dir = ref[">"]
            if grid[loc] == ref["\\"]:
                self.dir = ref["<"]
            if grid[loc] == ref["+"]:
                self.turn.rotate(1)
                if self.turn[0] == "l":
                    self.dir = ref["<"]
                if self.turn[0] == "r":
                    self.dir = ref[">"]
                    
        elif self.dir == ref["v"]:
            if grid[loc] == ref["/"]:
                self.dir = ref["<"]
            if grid[loc] == ref["\\"]:
                self.dir = ref[">"]
            if grid[loc] == ref["+"]:
                self.turn.rotate(1)
                if self.turn[0] == "l":
                    self.dir = ref[">"]
                if self.turn[0] == "r":
                    self.dir = ref["<"]
                    
        elif self.dir == ref["<"]:
            if grid[loc] == ref["/"]:
                self.dir = ref["v"]
            if grid[loc] == ref["\\"]:
                self.dir = ref["^"]
            if grid[loc] == ref["+"]:
                self.turn.rotate(1)
                if self.turn[0] == "l":
                    self.dir = ref["v"]
                if self.turn[0] == "r":
                    self.dir = ref["^"]
                    
        elif self.dir == ref[">"]:
            if grid[loc] == ref["/"]:
                self.dir = ref["^"]
            if grid[loc] == ref["\\"]:
                self.dir = ref["v"]
            if grid[loc] == ref["+"]:
                self.turn.rotate(1)
                if self.turn[0] == "l":
                    self.dir = ref["^"]
                if self.turn[0] == "r":
                    self.dir = ref["v"]



def collide():
    locs = {}
    for cart in sorted(carts):
        if locs.get(tuple(cart.loc)):
            return tuple(cart.loc)
        else:
            locs[tuple(cart.loc)] = True
    return None

def removecollided(loc):
    for cart in reversed(carts):
        if tuple(cart.loc) == loc:
            carts.remove(cart)

def tick():
    col = None
    for cart in list(sorted(carts)):
        if cart is not None:
            cart.move()
            cart.checkdir()
            col = collide()
            if col is not None:
                removecollided(col)

def collect():
    for cart in carts:
        frame[tuple(cart.loc)] = 255
    im = plt.imshow(frame)
    writer.grab_frame()

with open("input13.txt") as infile:
    grid = np.array([list(i) for i in infile.read().translate(tr).splitlines()], dtype=np.int32)
    
initcarts = {p:grid[p] for p in list(zip(*np.where(grid > 5)))}
carts = [Cart(p, grid[p]) for p in list(zip(*np.where(grid > 5)))]
grid[grid > 7] = 2
grid[grid > 5] = 1
frame = np.copy(grid)

fig = plt.figure()

metadata = dict(title='Day 13 anim', artist='me',
                comment='hackjob')
writer = animation.FFMpegWriter(fps=15, metadata=metadata)

#with writer.saving(fig, "writer_test.mp4", 150):
col = None
ticks = 0
while len(carts) > 1:
    tick()
    #collect()
    ticks += 1
    
print(ticks+1, tuple(carts[0].loc))

