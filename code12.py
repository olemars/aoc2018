# -*- coding: utf-8 -*-

tr = str.maketrans("#.", "10")
rtr = str.maketrans("10", "#.")
                    
with open("input12.txt") as infile:
    initialstate = [int(c) for c in infile.readline().lstrip("initial state: ").rstrip("\n").translate(tr)]  
    infile.readline()
    patterns = {int(a, 2):int(b, 2) for a,b in [s.split(" => ") for s in infile.read().translate(tr).splitlines()]}
    
    prepended = 0
    lastgen = initialstate
    currentgen = []
    totals = []
    for g in range(500):
        for i in range(-2,len(lastgen)+2):
            curval = 0
            for a in lastgen[max(0,i-2):min(len(lastgen), i+3)]:
                curval += a
                curval <<= 1
            curval >>= 1
            if i < 0:
                if patterns.get(curval, 0) or len(currentgen) > 0:
                    currentgen.append(patterns.get(curval, 0))
                    prepended += 1
            elif i > len(lastgen) - 3:
                trail = i - (len(lastgen) - 3)
                curval <<= trail
                currentgen.append(patterns.get(curval, 0))
            else:
                currentgen.append(patterns.get(curval, 0))
                
        while currentgen[-1] == 0:
            currentgen.pop()
            
        lastgen = currentgen
        currentgen = []
    
        
        total = 0
        for i in range(len(lastgen)):
            total += (i-prepended) * lastgen[i]
        totals.append(total)
    print(totals[19])    
    print(totals[200] + ((totals[300]-totals[200])/100)*(50000000000-201))