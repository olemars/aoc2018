# -*- coding: utf-8 -*-

with open("input2.txt", "r") as infile:
    l = infile.read().split("\n")
    asc = []
    for uid in l:
        if len(uid) > 0:
            chars = []
            for c in uid:
                chars.append(ord(c))
            asc.append(chars)    
     
    code1 = []
    code2 = []
    codemask = []
    for i in range(len(asc)-1):
        for j in range(i+1, len(asc)):
            x = [a-b != 0 for a,b in zip(asc[i], asc[j])]
            if sum(x) == 1:
                code1 = asc[i]
                code2 = asc[j]
                codemask = x
                break
        if len(code1) > 0:
            break
    code = ""
    for i in range(len(codemask)):
        if not codemask[i]:
           code += (chr(code1[i]))

    print(code)