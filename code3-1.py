# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

data = np.zeros(shape=(1000, 1000), dtype=np.uint16)
idmap= []
raw = []

with open("input3.txt", "r") as infile:
    raw = infile.read().rstrip("\n").split("\n")
    
for s in raw:
    uid = s[s.find("#")+1:s.find("@")].rstrip()
    oris, dims = s[s.find("@") + 2:].split(": ")
    x, y = (int(a) for a in oris.split(","))
    w, h = (int(a) for a in dims.split("x"))
    data[y:y+h, x:x+w] += 1
    idmap.append(uid)
    
plt.imshow(data, vmin=0, vmax=8)
print((data>1).sum())