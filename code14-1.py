# -*- coding: utf-8 -*-

from collections import deque
from itertools import islice
from math import floor

tests = (5,9,18,2018)

runlen = 0
with open("input14.txt") as infile:
    runlen = int(infile.read())

numelves = 2
elves = {i:i for i in range(numelves)}
anslen = 10
init = [3,7]
results = deque(init)

for i in range(2,runlen + anslen+1):
    score = 0
    for elf in elves:
        elves[elf] = (elves[elf] + 1 + results[elves[elf]]) % len(results)
        score += results[elves[elf]]
        
    pending = []    
    while score > 0:
        pending.append(score % 10)
        score = floor(score / 10)
    pending.reverse()
    for x in pending:
        results.append(x)
    if len(pending) == 0:
        results.append(0)
        
    #test
    if i-anslen in tests:
        print(i-anslen,list(islice(results, i-anslen, i)))
    if i-anslen == runlen:
        print(i-anslen,list(islice(results, i-anslen, i)))
        print("".join(str(x) for x in list(islice(results, i-anslen, i))))