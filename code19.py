# -*- coding: utf-8 -*-

from CPU import CPU


with open("input19.txt") as infile:
    ip = int(infile.readline().rstrip().lstrip("#ip "))
    asm = [line.split(" ") for line in infile.read().splitlines()]
    program = [[line[0], int(line[1]), int(line[2]), int(line[3])] for line in asm]
    instrcount = len(program)
    
    cpu = CPU(6, ip)
    while 0 <= cpu.ipval < instrcount:       
        line = program[cpu.ipval]
        instr, a, b, c = line[0], line[1], line[2], line[3]
        cpu.table[instr](a,b,c)
       # print(cpu.reg)
        
    print("p1: ", cpu.reg[0])
    
#    cpu = CPU(6, ip)
#    cpu.reg[0] = 1
#    while 0 <= cpu.ipval < instrcount:       
#        line = program[cpu.ipval]
#        instr, a, b, c = line[0], line[1], line[2], line[3]
#        cpu.table[instr](a,b,c)
#        print(cpu.reg)
#        
#    print("p2: ", cpu.reg[0])
    
    #the asm program is trying to very inefficiently factorize a very large number
    #the check happens at instruction 4, eqrr 1 2 1 
    
    cpu = CPU(6, ip)
    cpu.reg[0] = 1
    while cpu.ipval != 4:       
        line = program[cpu.ipval]
        instr, a, b, c = line[0], line[1], line[2], line[3]
        cpu.table[instr](a,b,c)
        #print(cpu.reg)
    
    bignum = cpu.reg[2] #extract the number from reg 2
    target = 0
    for i in range(1,bignum+1):
        if (bignum % i) == 0:
            target += i 
    print("p2: ", target)