# -*- coding: utf-8 -*-

raw = []

with open("input5.txt") as infile:
    raw = list(infile.read().rstrip("\n"))

i = 0
count = 0
while True:
    pair = raw[i:i+2]
    if pair[1] == pair[0].swapcase():
        del raw[i:i+2]
        count += 1
        i -= 1
        if i < 0:
            i = 0
        continue
    i += 1
    if i >= len(raw)-1:
        break
    
print(len(raw))


