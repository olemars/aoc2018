# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
from PIL import Image
import pytesseract
import cv2
import re
import matplotlib.pyplot as plt

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'
tessdata_dir_config = r'--tessdata-dir "C:\Program Files (x86)\Tesseract-OCR\tessdata"'

raw = []
pos = []
vel = []
with open("input10.txt") as infile:
    raw = [re.findall("<.*?>", s) for s in infile.readlines()]
    
    pos = [tuple([int(s[0]), int(s[1])]) for s in [l[0].strip("< >").split(", ") for l in raw]]
    vel = [tuple([int(s[0]), int(s[1])]) for s in [l[1].strip("< >").split(", ") for l in raw]]
    npos = np.array(pos)
    nvel = np.array(vel)
    ffw = npos + (nvel*0)
    dimy = ffw[:,1].max() - ffw[:,1].min()
    mini = 0
    i = 0
    while True:
        ffw = npos + (nvel*i)
        if ffw[:,1].max() - ffw[:,1].min() <= dimy:
            dimy = ffw[:,1].max() - ffw[:,1].min()
            mini = i
        else:
            break
        i += 1
    
    print(mini)
    #plt.ion()
    ffw = npos + (nvel*(mini))
    arr = np.zeros(shape=(1 + ffw[:,1].max() - ffw[:,1].min(), 1 + ffw[:,0].max() - ffw[:,0].min()), dtype=np.int32)
    for c in ffw:
        arr[c[1] - ffw[:,1].min() ,c[0] - ffw[:,0].min()] = 255
    plt.imshow(arr)
#    for i in range(mini-50, mini+50):
#        ffw = npos + (nvel*(i))
#        arr = np.zeros(shape=(1 + ffw[:,1].max() - ffw[:,1].min(), 1 + ffw[:,0].max() - ffw[:,0].min()), dtype=np.int32)
#        for c in ffw:
#            arr[c[1] - ffw[:,1].min() ,c[0] - ffw[:,0].min()] = 255
#        s = pytesseract.image_to_string(arr, config=tessdata_dir_config)
#        if len(s) > 1:
#            break
#        print(i)
#        plt.imshow(arr)
#        display.clear_output(wait=True)
#        display.display(plt.gcf())
#    print(s)