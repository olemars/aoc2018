# -*- coding: utf-8 -*-

import functools

class CPU:
    def __init__(self, regcount = 4, ip = None):
        self.reg = [0]*regcount
        self.ip = ip
        self.ipval = 0
        
        self.instr = [self.addr, self.addi, self.mulr, self.muli, self.banr, self.bani,
                      self.borr, self.bori, self.setr, self.seti,
                      self.gtir, self.gtri, self.gtrr,
                      self.eqir, self.eqri, self.eqrr]
        self.opcode = {i:None for i in range(16)}
        self.table = {"addr":self.addr, "addi":self.addi, "mulr":self.mulr, "muli":self.muli,
                      "banr":self.banr, "bani":self.bani, "borr":self.borr, "bori":self.bori,
                      "setr":self.setr, "seti":self.seti, "gtir":self.gtir, "gtri":self.gtri,
                      "gtrr":self.gtrr, "eqir":self.eqir, "eqri":self.eqri, "eqrr":self.eqrr}
        
    def ipdecorator(func):
        @functools.wraps(func)
        def instruction_wrapper(self, *args, **kwargs):
            if self.ip is not None:
                self.reg[self.ip] = self.ipval
            func(self, *args, **kwargs)
            if self.ip is not None:
                self.ipval = self.reg[self.ip]
                self.ipval += 1
        return instruction_wrapper
    
    @ipdecorator    
    def addr(self, a, b, c):
        self.reg[c] = self.reg[a] + self.reg[b]
    
    @ipdecorator
    def addi(self, a, b, c):
        self.reg[c] = self.reg[a] + b
    
    @ipdecorator
    def mulr(self, a, b, c):
        self.reg[c] = self.reg[a] * self.reg[b]
    
    @ipdecorator
    def muli(self, a, b, c):
        self.reg[c] = self.reg[a] * b
   
    @ipdecorator
    def banr(self, a, b, c):
        self.reg[c] = self.reg[a] & self.reg[b]
    
    @ipdecorator
    def bani(self, a, b, c):
        self.reg[c] = self.reg[a] & b
    
    @ipdecorator
    def borr(self, a, b, c):
        self.reg[c] = self.reg[a] | self.reg[b]
    
    @ipdecorator
    def bori(self, a, b, c):
        self.reg[c] = self.reg[a] | b
    
    @ipdecorator
    def setr(self, a, b, c):
        self.reg[c] = self.reg[a]
   
    @ipdecorator
    def seti(self, a, b, c):
        self.reg[c] = a
    
    @ipdecorator
    def gtir(self, a, b, c):
        self.reg[c] = 1 if a > self.reg[b] else 0
    
    @ipdecorator
    def gtri(self, a, b, c):
        self.reg[c] = 1 if self.reg[a] > b else 0
    
    @ipdecorator
    def gtrr(self, a, b, c):
        self.reg[c] = 1 if self.reg[a] > self.reg[b] else 0
    
    @ipdecorator
    def eqir(self, a, b, c):
        self.reg[c] = 1 if a == self.reg[b] else 0
    
    @ipdecorator
    def eqri(self, a, b, c):
        self.reg[c] = 1 if self.reg[a] == b else 0
    
    @ipdecorator
    def eqrr(self, a, b, c):
        self.reg[c] = 1 if self.reg[a] == self.reg[b] else 0