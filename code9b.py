# -*- coding: utf-8 -*-

from collections import deque

class Game:
    def __init__(self, nplayers, nmarbles):
        self.players = [0] * nplayers
        self.table = deque([0])
        self.nplayers = nplayers
        self.nmarbles = nmarbles
        
    def play(self):
        nextMarble = 0        
        while nextMarble < self.nmarbles:
            for i in range(nplayers):
                nextMarble += 1
                if nextMarble > self.nmarbles:
                    break
                
                if nextMarble % 23 != 0:
                    self.table.rotate(-1)
                    self.table.append(nextMarble)
                else:
                    self.players[i] += nextMarble
                    self.table.rotate(7)
                    self.players[i] += self.table.pop()
                    self.table.rotate(-1)
                
        return max(self.players)
    

raw = []
with open("input9.txt") as infile:
    raw = infile.read().rstrip("\n").splitlines()

for s in raw:
    nplayers = int(s.split()[0])
    nmarbles = int(s.split()[6])
    
    game = Game(nplayers, nmarbles)
    print(game.play())
    
    game = Game(nplayers, nmarbles*100)
    print(game.play())