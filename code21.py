# -*- coding: utf-8 -*-

from CPU import CPU
import math

with open("input21.txt") as infile:
    ip = int(infile.readline().rstrip().lstrip("#ip "))
    asm = [line.split(" ") for line in infile.read().splitlines()]
    program = [[line[0], int(line[1]), int(line[2]), int(line[3])] for line in asm]
    instrcount = len(program)
    
    first = None
    cpu = CPU(6, ip)
    while 0 <= cpu.ipval < instrcount:       
        line = program[cpu.ipval]
        if cpu.ipval == 28:
            first = cpu.reg[2]
            print("p1: ", cpu.reg[2]) # reg 0 is only used in one code line, checking for eq with reg 2
            break
        instr, a, b, c = line[0], line[1], line[2], line[3]
        cpu.table[instr](a,b,c)
    
#the emulated asm program is slow, especially finding a divisor for one of the constants, 
#but it's much faster if implemented directly. the RNG eventually loops, find the last value in the loop
    c = 0
    r = 0
    hit = 0
    last = 0
    vals = {}
    while True:
        last = r
        c = r | 65536
        r = 4843319
        #r += c % 256
        r = (r + c % 256) % 16777216 * 65899 % 16777216
        #r %= 16777216
        #r *= 65899
        #r %= 16777216
        while c >= 256:
            c = math.floor(c / 256)
            #r += c % 256
            r = (r + c % 256) % 16777216 * 65899 % 16777216
            #r %= 16777216
            #r *= 65899
            #r %= 16777216
        
        if vals.get(r):
            print("p2: ", last)
            break
        else:
            vals[r] = True