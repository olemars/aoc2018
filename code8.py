# -*- coding: utf-8 -*-


class Node:
    def __init__(self):
        self.children = []
        self.metadata = []
        
    def getSum(self):
        childSum = sum(c.getSum() for c in self.children)
        return childSum + sum(self.metadata)
    
    def getValue(self):
        if len(self.children) == 0:
            return sum(self.metadata)
        
        value = 0
        for m in self.metadata:
            if m > 0 and m <= len(self.children):
                value += self.children[m-1].getValue()
        return value                
    
    def processData(self, data):
        numChildren, numMetadata = data [0:2]
        del data[0:2]
        for i in range(numChildren):
            child = Node()
            data = child.processData(data)
            self.children.append(child)
        for i in range(numMetadata):
            self.metadata.append(data[i])
        del data[:numMetadata]
            
        return data

raw = []
with open("input8.txt") as infile:
    raw = [int(x) for x in infile.read().rstrip("\n").split(" ")]
    
rootNode = Node()
rootNode.processData(raw)

print(rootNode.getSum())
print(rootNode.getValue())