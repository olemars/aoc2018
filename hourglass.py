# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 23:16:28 2018

@author: OleMartin
"""
import math

def printline(length, x):
    line = ""
    for j in range(x):
        line += "X"
    for j in range(x, length-x):
        line +="@"
    for j in range(length-x, length):
        line += "X"
    print(line)
    
def hourglass(n):
    if not n % 2:
        return
    for i in range(math.floor(n/2)):
        printline(n, i)
    for i in range(math.floor(n/2), -1, -1):
        printline(n, i)
        
        
hourglass(25)