# -*- coding: utf-8 -*-
import bisect 
raw = []

with open("input7.txt") as infile:
    raw = infile.read().splitlines()
    
class Step:
    def __init__(self, stepid):
        self.stepid = stepid
        self.prev = []
        self.next = []
        self.done = False
        
    def __lt__(self, other):
        return self.stepid < other.stepid
    def __le__(self, other):
        return self.stepid <= other.stepid
    def __gt__(self, other):
        return self.stepid > other.stepid
    def __ge__(self, other):
        return self.stepid >= other.stepid
        
    def isready(self):
        for s in self.prev:
            if s.done == False:
                return False
        return True
    
pending = {}

for s in raw:
    stepid = s[s.find("Step ") + 5]
    nextstepid = s[s.find("step ") + 5]
    if pending.get(stepid):
        step = pending[stepid]
    else:
        step = Step(stepid)
        pending[stepid] = step
    
    if pending.get(nextstepid):
        nextstep = pending[nextstepid]
    else:
        nextstep = Step(nextstepid)
        pending[nextstepid] = nextstep
    
    step.next.append(nextstep)
    nextstep.prev.append(step)
    
ready = []
done = []
order = ""

changes = 1
while len(pending.items()) > 0 or len(ready) > 0:
    changes = 0
    for s in list(pending):
        if pending[s].isready():
            bisect.insort_left(ready, pending.pop(s))
            changes += 1
    
    if len(ready) > 0:
        changes += 1
        donestep = ready.pop(0)
        donestep.done = True
        done.append(donestep)
        order += donestep.stepid
    if changes == 0:
        break
    
    
print(order)
    