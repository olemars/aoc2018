# -*- coding: utf-8 -*-

import numpy as np
from collections import deque
from matplotlib import pyplot as plt

sand = 0
still = 80
running = 150
clay=255

def extractrange(s, prefix, rsep):
    s = s.lstrip(prefix)
    if rsep in s:
        rmin, rmax = [int(i) for i in s.split(rsep)]
    else:
        rmin = int(s)
        rmax = rmin
    return rmin, rmax
    
def flow(grid, source):
    global sand, still, running, clay
    global xmin, xmax, ymin, ymax
    obstacle = [still, clay]
    flowable = [sand, running]
    front = deque([source])
    while len(front) > 0:
        current = front.popleft()
        state = grid[current]
        if current is None or state in obstacle:
            continue
        
        down = current[0]+1, current[1]
        
        if state == sand:
            grid[current] = running
        if down[0] < ymax+1:  
            #down first
            if grid[down] not in obstacle:
                if grid[down] in flowable:
                    front.append(down)
            # fill bucket
            else:
                front += fill(grid, current)

def fill(grid, loc):
    x = loc[1]
    y = loc[0]
    sinks = []
    
    while len(sinks) == 0:
        sinks = fillrow(grid, (y,x))
        y -= 1
    return sinks
    
def fillrow(grid, loc):
    global sand, still, running, clay, sloshing
    obstacle = [still, clay]
    flowable = [sand, running]
    sinks = []
    leftend = None
    rightend = None
    
    current = loc
    while leftend is None:
        grid[current] = running
        left = (current[0], current[1]-1)
        down = current[0]+1, current[1]
        if grid[down] in flowable:
            leftend = (current[1], grid[down])
            sinks.append(current)
        elif grid[left] in obstacle:
            leftend = (current[1], grid[left])
        current = left
    
    current = loc
    while rightend is None:
        grid[current] = running
        right = (current[0], current[1]+1)
        down = current[0]+1, current[1]
        if grid[down] in flowable:
            rightend = (current[1], grid[down])
            sinks.append(current)
        elif grid[right] in obstacle:
            rightend = (current[1], grid[right])
        current = right
        
    if leftend[1] in obstacle and rightend[1] in obstacle:
        grid[loc[0], leftend[0]:rightend[0]+1] = still
    
    return sinks    
    
with open("input17.txt") as infile:
    raw = [l.split(", ") for l in infile.read().splitlines()]
    scans = []
    xmin, xmax, ymin, ymax = None, None, None, None
    for scan in raw:
        for rg in scan:
            if "y=" in rg:
                ystart, yend = extractrange(rg, "y=", "..")
            elif "x=" in rg:
                xstart, xend = extractrange(rg, "x=", "..")
        scans.append(((ystart, yend), (xstart, xend)))
        if xmin is None or xstart < xmin: xmin = xstart
        if xmax is None or xend > xmax: xmax = xend
        if ymin is None or ystart < ymin: ymin = ystart
        if ymax is None or yend > ymax: ymax = yend
    
    grid = np.zeros(shape=(ymax + 2, xmax + 2), dtype=np.int16)
    for scan in scans:
        grid[scan[0][0]:scan[0][1]+1,scan[1][0]:scan[1][1]+1] = clay
        
    plt.axis("off")
    flow(grid, (0,500))
    plt.imshow(grid[ymin-1:ymax+1,xmin-1:xmax+1], aspect="auto", cmap="viridis")
    plt.imsave("d17after.png", grid[ymin-1:ymax+1,xmin-1:xmax+1], cmap="viridis")
    print("p1: ", (grid[ymin:ymax+1,xmin-1:xmax+1] == running).sum() + (grid[ymin:ymax+1,xmin-1:xmax+1] == still).sum())
    print("p2: ", (grid[ymin:ymax+1,xmin-1:xmax+1] == still).sum())
    