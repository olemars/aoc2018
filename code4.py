# -*- coding: utf-8 -*-


from datetime import datetime
import numpy as np

raw = []
with open("input4.txt") as infile:
    raw = infile.read().rstrip("\n").split("\n")
    
entries = {}
for s in raw:
    entries[datetime.strptime(s[s.find("[")+1:s.find("]")], "%Y-%m-%d %H:%M")] = s[s.find("]")+1:].lstrip()
    
guards = {}
curguard = ""
for dt in sorted(entries.keys()):
    if entries[dt].startswith("Guard"):
        curguard = entries[dt][entries[dt].find("#")+1:].split(" ")[0]
        if guards.get(curguard) is None:
            guards[curguard] = np.zeros(shape=(60), dtype=np.int32)
    elif "falls" in entries[dt]:
        guards[curguard][dt.minute:] += 1
    elif "wakes" in entries[dt]:
        guards[curguard][dt.minute:] -= 1
        guards[curguard][guards[curguard] < 0] = 0 


#ans 1
maxguard = ""
maxsleep = 0
maxmin = 0
for g in guards:
    if guards[g].sum() > maxsleep:
        maxguard = g
        maxsleep = guards[g].sum()
        maxmin = guards[g].argmax()
checksum = int(maxguard) * maxmin
print(f"Guard: {maxguard}, sleep: {maxsleep}, max: {maxmin}, checksum: {checksum}")


#ans 2
maxguard = ""
maxsleep = 0
maxmin = 0
maxmax = 0
for g in guards:
    if guards[g].max() > maxmax:
        maxguard = g
        maxsleep = guards[g].sum()
        maxmin = guards[g].argmax()
        maxmax = guards[g].max()
checksum = int(maxguard) * maxmin
print(f"Guard: {maxguard}, sleep: {maxsleep}, max: {maxmin}, checksum: {checksum}")