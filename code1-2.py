# -*- coding: utf-8 -*-


with open("input1.txt", "r") as infile:
    l = infile.read().strip().split("\n")
    freqs = {}
    freq = 0
    keeplooping = True
    while(keeplooping):
        for f in l:
            freq += int(f)
            if freqs.get(freq) is not None:
                print(freq)
                keeplooping = False
            freqs[freq] = True
            if keeplooping is False:
                break