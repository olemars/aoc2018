# -*- coding: utf-8 -*-

with open("input2.txt", "r") as infile:
    l = infile.read().split("\n")
    
    doubles = 0
    triples = 0
    for uid in l:
        chars = {}
        for c in uid:
            if chars.get(c) is not None:
                chars[c] += 1
            else:
                chars[c] = 1
        hasdouble = False
        hastriple = False
        for x in chars.values():
            if x == 2:
                hasdouble = True
            if x == 3:
                hastriple = True
        if hasdouble:
            doubles += 1
        if hastriple:
            triples += 1
        
    print(doubles * triples)