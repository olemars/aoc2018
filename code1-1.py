# -*- coding: utf-8 -*-


with open("input1.txt", "r") as infile:
    freq = sum([int(f) for f in infile.read().strip().split("\n")])
    print(freq)