# -*- coding: utf-8 -*-

import numpy as np
from collections import deque

class Fighter:
    def __init__(self, loc, side, power):
        self.loc = loc
        self.side = side
        self.hp = 200
        self.power = power
 
    def __lt__(self, other):
        return self.loc < other.loc
    def __le__(self, other):
        return self.loc <= other.loc
    def __gt__(self, other):
        return self.loc > other.loc
    def __ge__(self, other):
        return self.loc >= other.loc
    def __eq__(self, other):
        return self.loc == other.loc
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def turn(self):
        global pf
        if self.hp <= 0:
            return None
        
        targets = self.checksurroundings()
        if len(targets) > 0:
            self.attack(targets)
        else:
            self.move(pf.getbeststeptotarget(self.side, self.loc))
            targets = self.checksurroundings()
            if len(targets) > 0:
                self.attack(targets)
        
    def checksurroundings(self):
        global pf
        global fighters
        neighbors = pf.getneighbors(self.loc, True)
        targets = []
        if len(neighbors) > 0:
            for f in sorted(fighters):
                if f.side != self.side and f.hp > 0:
                    for n in neighbors:
                        if f.loc == n:
                            targets.append(f)
        return targets
            
    def move(self, loc):
        global grid
        if loc is not None:
            grid[self.loc] = "."
            self.loc = loc
            grid[self.loc] = self.side
        
    def attack(self, targets):
        hp = 201
        weakest = None
        for t in targets:
            if t.hp < hp:
                hp = t.hp
                weakest = t
        if weakest is not None:
            weakest.takedamage(self.power)
                
    
    def takedamage(self, dmg):
        self.hp -= dmg
        if self.hp <= 0:
            self.die()
            
    def die(self):
        print(self.side, self.loc, self.power, self.hp)
        grid[self.loc] = "."
            
    
class Pathfinder:
    def __init__(self, grid):
        self.nodes = list(zip(*np.where(grid != "#")))
        self.dirs = [(-1,0), (0,-1),(0,1),(1,0)]
    
    def getneighbors(self, loc, includeenemies = False):
        global grid
        locs = [tuple([loc[0]+d[0], loc[1]+d[1]]) for d in self.dirs]
        if includeenemies:
            return [l for l in locs if l in self.nodes]
        return [l for l in locs if l in self.nodes and grid[l] == "."]
                      
    def getbeststeptotarget(self, ownside, loc):
        global fighters
        graph = self.bfs(loc)
        targets = [f for f in sorted(fighters) if f.side != ownside and f.hp > 0]
        shortest = -1
        shortestpath = []
        for t in targets:
            neighbors = self.getneighbors(t.loc)
            for n in neighbors:
                length, path = self.path(graph, loc, n)
                if length > 0 and (shortest < 0 or length < shortest):
                    shortest = length
                    shortestpath = path
        if len(shortestpath) > 0:
            return shortestpath[0]
        return None
    
    def bfs(self, loc):
        front = deque([loc])
        graph = {loc:None}
        
        while len(front) > 0:
            current = front.popleft()
            neighbors = self.getneighbors(current)
            for n in neighbors: 
                if n not in graph:
                    front.append(n)
                    graph[n] = current
        
        return graph
    
    def path(self, graph, start, end):
        current = end
        path = []
        length = 0
        while current != start:
            length += 1
            path.append(current)
            current = graph.get(current)
            if current is None:
                return -1, []
        path.reverse()
        return length, path
        
grid = None
sides = ("E", "G")
with open("input15.txt") as infile:
    base = np.array([list(i) for i in infile.read().splitlines()], dtype=np.str)
    grid = base.copy()
    fighters = [Fighter(p, grid[p], 3) for p in list(zip(*np.where((grid == "G") | (grid == "E"))))]
    pf = Pathfinder(grid)
    
    i = 0
    end = False
    while not end:
        j=1
        for f in sorted(fighters):
            j += 1
            f.turn()
            end = False
            for s in sides:
                end |= not np.any(grid == s)
            if end:
                if j == len(fighters):
                    i += 1
                sumhp = 0
                for f in fighters:
                    if f.hp > 0:
                        sumhp += f.hp
                print("p1: ",i*sumhp)
                break
        i += 1
        
    pow = 4
    reallyend = False
    while not reallyend:
        print(pow)
        grid = base.copy()
        fighters = [Fighter(p, grid[p], 3) for p in list(zip(*np.where((grid == "G") | (grid == "E"))))]
        pf = Pathfinder(grid)
        startelves = (grid == "E").sum()
        i = 0
        end = False
        for f in fighters:
            if f.side == "E":
                f.power = pow
        
        while not end:
            j=1
            for f in sorted(fighters):
                j += 1
                f.turn()
                end = (grid == "E").sum() < startelves
                for s in sides:
                    end |= not np.any(grid == s)
                if end:
                    if j == len(fighters):
                        i += 1
                    if (grid == "E").sum() == startelves and (grid == "G").sum() == 0:
                        sumhp = 0
                        for f in fighters:
                            if f.hp > 0:
                                sumhp += f.hp
                        print("p2: ",i*sumhp)
                        reallyend = True
                    break
            i += 1
        pow += 1