# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

def mhd(p, q):
    return abs((p[0] - q[0])) + abs((p[1] - q[1]))


raw = []
points = []
with open("input6.txt") as infile:
    raw = infile.read().rstrip("\n").split("\n")


for s in raw:
    points.append(tuple(int(x) for x in s.split(", ")))
    
minx = sorted(points, key=lambda f: f[0])[0][0]
maxx = sorted(points, key=lambda f: f[0])[-1][0]
miny = sorted(points, key=lambda f: f[1])[0][1]
maxy = sorted(points, key=lambda f: f[1])[-1][1]

areas = [0] * len(points)
mhfield = np.zeros(shape=(maxx-minx,maxy-miny), dtype=np.int32)

for x in range(minx, maxx+1):
    for y in range(miny, maxy+1):
        nearestdist = None
        nearestpoint = None
        for i in range(len(points)):
            dist = mhd((x,y), points[i])
            if nearestdist is None or dist < nearestdist:
                nearestdist = dist
                nearestpoint = i
            elif dist == nearestdist:
                nearestpoint = -1
        if x == minx or y == miny or x == maxx or y == maxy:
            areas[nearestpoint] = -1 #infinite
        elif areas[nearestpoint] >= 0:
            areas[nearestpoint] += 1
        mhfield[x-minx-1,y-miny-1] = nearestpoint if nearestpoint >= 0 else 255
maxareaidx = np.array(areas).argmax()
print("Point with max area")
print(points[maxareaidx])
print(areas[maxareaidx])
plt.imshow(mhfield)

saferegionfield = np.zeros(shape=(maxx-minx,maxy-miny), dtype=np.int32)
regionsize = 0
for x in range(minx, maxx+1):
    for y in range(miny, maxy+1):
        distsum = 0
        for i in range(len(points)):
            dist = mhd((x,y), points[i])
            distsum += dist
        if distsum < 10000:
            regionsize += 1
            saferegionfield[x-minx,y-miny] = 1
            
print("Safe region size")
print(regionsize)
plt.imshow(saferegionfield)