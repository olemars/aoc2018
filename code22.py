# -*- coding: utf-8 -*-

import numpy as np
import heapq
   
def addcolumn():
    global edgecol, edgerow
    j = edgecol
    for i in range(edgerow):
        if (i == 0 and j == 0) or ((i,j) == target):
            geologic[i,j] = 0
        elif i == 0:
            geologic[i,j] = 16807 * j
        elif j == 0:
            geologic[i,j] = 48271 * i
        else:
            geologic[i,j] = erosion[i,j-1]*erosion[i-1,j]
            
        erosion[i,j] = (geologic[i,j] + depth) % 20183
        risk[i,j] = erosion[i,j] % 3
    edgecol += 1
    
def addrow():
    global edgecol, edgerow
    i = edgerow
    for j in range(edgecol):
        if (i == 0 and j == 0) or ((i,j) == target):
            geologic[i,j] = 0
        elif i == 0:
            geologic[i,j] = 16807 * j
        elif j == 0:
            geologic[i,j] = 48271 * i
        else:
            geologic[i,j] = erosion[i,j-1]*erosion[i-1,j]
            
        erosion[i,j] = (geologic[i,j] + depth) % 20183
        risk[i,j] = erosion[i,j] % 3
    edgerow += 1
    
def astar(target):
    front = heapq([(0, (0,0))])

with open("input22.txt") as infile:
    depth = int(infile.readline().lstrip("depth: ").rstrip("\n"))
    target = tuple(int(x) for x in reversed(infile.readline().lstrip("target: ").rstrip("\n").split(",")))
    
    erosion = np.zeros(shape=(max(target)*2, max(target)*2), dtype=np.int32)
    geologic = np.zeros(shape=(max(target)*2, max(target)*2), dtype=np.int32)
    risk = np.zeros(shape=(max(target)*2, max(target)*2), dtype=np.int32)
    edgerow = target[0]+1
    edgecol = target[1]+1
    for i in range(edgerow):
        for j in range(edgecol):
            if (i == 0 and j == 0) or ((i,j) == target):
                geologic[i,j] = 0
            elif i == 0:
                geologic[i,j] = 16807 * j
            elif j == 0:
                geologic[i,j] = 48271 * i
            else:
                geologic[i,j] = erosion[i,j-1]*erosion[i-1,j]
                
            erosion[i,j] = (geologic[i,j] + depth) % 20183
            risk[i,j] = erosion[i,j] % 3
            
    print("p1: ", risk[:edgerow,:edgecol].sum())
    
    