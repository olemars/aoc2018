# -*- coding: utf-8 -*-

from collections import deque

rooms = {}
roomcount = 0
class Room:
    def __init__(self, y,x):
        global rooms, roomcount
        rooms[(y,x)] = self
        roomcount += 1
        self.y = y
        self.x = x
        self.N = None
        self.S = None
        self.E = None
        self.W = None
        self.dist = 0
        #print(self.y,self.x)
        
def traverse(start, directions):
    cur = start
    while directions:
        c = directions.popleft()
        if c == "N":
            cur.N = rooms.get((cur.y-1, cur.x))
            if cur.N is None:
                cur.N = Room(cur.y-1, cur.x)
            cur.N.S = cur
            cur = cur.N
        elif c == "S":
            cur.S = rooms.get((cur.y+1, cur.x))
            if cur.S is None:
                cur.S = Room(cur.y+1, cur.x)
            cur.S.N = cur
            cur = cur.S
        elif c == "E":
            cur.E = rooms.get((cur.y, cur.x+1))
            if cur.E is None:
                cur.E = Room(cur.y, cur.x+1)
            cur.E.W = cur
            cur = cur.E
        elif c == "W":
            cur.W = rooms.get((cur.y, cur.x-1))
            if cur.W is None:
                cur.W = Room(cur.y, cur.x-1)
            cur.W.E = cur
            cur = cur.W
        elif c == "|":
            cur = start
        elif c == "(":
            cur = traverse(cur, directions)
        elif c == ")":
            return start
    return cur
            
def bfs(start):
    front = deque([start])
    graph = {start:None}
    dist = 0
    kpluscount = 0
    while len(front) > 0:
        current = front.popleft()
        if graph[current] is not None:
            current.dist = graph[current].dist + 1
        if current.dist > dist:
            dist = current.dist
        if current.dist >= 1000:
            kpluscount += 1
        neighbors = [current.N, current.E, current.S, current.W]
        for n in neighbors: 
            if n not in graph and n is not None:
                front.append(n)
                graph[n] = current
    
    return graph, dist, kpluscount
        
with open("input20.txt") as infile:
    raw = infile.read().strip()
    directions = deque([c for c in raw])
    origin = Room(0,0)
    end = traverse(origin, directions)
    path, maxdist, kpluscount = bfs(origin)
    
    print("p1 : ", maxdist)
    print("p2 : ", kpluscount)