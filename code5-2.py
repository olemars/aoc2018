# -*- coding: utf-8 -*-

raw = []

with open("input5.txt") as infile:
    raw = list(infile.read().rstrip("\n"))

data = []
res = {}
a = ord("a")
z = ord("z")
for c in range(a, z+1):
    data = [val for val in raw if val != chr(c) and val != chr(c).swapcase()]
    i = 0
    count = 0
    while True:
        if i >= len(data)-1:
            break
        pair = data[i:i+2]
        if pair[1] == pair[0].swapcase():
            del data[i:i+2]
            count += 1
            i -= 1
            if i < 0:
                i = 0
            continue
        i += 1
        
    res[chr(c)] = len(data)

print(sorted(res.items(), key=lambda x: x[1])[0])